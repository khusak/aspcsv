#include <fstream>
#include <iostream>
#include <queue>
#include <sstream>
#include <string>
#include <unordered_map>
#include <vector>

template <typename T>
class Values {
 public:
  void loadStringsFromFile(std::string filename, int columnIndex) {
    std::string line, word;
    std::fstream file(filename, std::ios::in);

    if (file.is_open()) {
      std::getline(file, line);
      while (std::getline(file, line)) {
        std::vector<std::string> columns = csv_read_row(line, ',');
        addValue(columns[columnIndex]);
        addKeyValue(std::stoll(columns[2]), columns[columnIndex]);
      }
    } else {
      std::cout << "Nije moguce procitati datoteku." << std::endl;
    }
  }

  void loadDoublesFromFile(std::string filename, int columnIndex) {    
    std::string line, word;
    std::fstream file(filename, std::ios::in);

    if (file.is_open()) {
      std::getline(file, line);
      while (std::getline(file, line)) {
        std::vector<std::string> columns = csv_read_row(line, ',');
        addValue(std::stod(columns[columnIndex]));
        addKeyValue(std::stoll(columns[2]), std::stod(columns[columnIndex]));
      }
    } else {
      std::cout << "Nije moguce procitati datoteku." << std::endl;
    }
  }

  void loadIntsFromFile(std::string filename, int columnIndex) {
    std::string line, word;
    std::fstream file(filename, std::ios::in);

    if (file.is_open()) {
      std::getline(file, line);
      while (std::getline(file, line)) {
        std::vector<std::string> columns = csv_read_row(line, ',');
        addValue(std::stoi(columns[columnIndex]));
        addKeyValue(std::stoll(columns[2]), std::stoi(columns[columnIndex]));
      }
    } else {
      std::cout << "Nije moguce procitati datoteku." << std::endl;
    }
  }

  std::vector<std::string> csv_read_row(std::string &line, char delimiter) {
    std::stringstream ss(line);
    return csv_read_row(ss, delimiter);
  }

  std::vector<std::string> csv_read_row(std::istream &in, char delimiter) {
    std::stringstream ss;
    bool inquotes = false;
    std::vector<std::string> row;  // relying on RVO
    while (in.good()) {
      char c = in.get();
      if (!inquotes && c == '"')  // beginquotechar
      {
        inquotes = true;
      } else if (inquotes && c == '"')  // quotechar
      {
        if (in.peek() == '"')  // 2 consecutive quotes resolve to 1
        {
          ss << (char)in.get();
        } else  // endquotechar
        {
          inquotes = false;
        }
      } else if (!inquotes && c == delimiter)  // end of field
      {
        row.push_back(ss.str());
        ss.str("");
      } else if (!inquotes && (c == '\r' || c == '\n')) {
        if (in.peek() == '\n') {
          in.get();
        }
        row.push_back(ss.str());
        return row;
      } else {
        ss << c;
      }
    }
    return row;
  }

  void addValue(T value) {
    values.push_back(value);
    valuesPriority.push(value);
  }

  void addKeyValue(long long key, T value) { valuesKeys.insert({key, value}); }

  int size() { return values.size(); }

  void top(int n) {
    for (int i = 0; i < n; i++) {
      std::cout << valuesPriority.top() << std::endl;
      valuesPriority.pop();
    }
  }

  void show() {
    while (!valuesPriority.empty()) {
      std::cout << valuesPriority.top() << ' ';
      valuesPriority.pop();
    }
  }

  T getValueByKey(long long key) { return valuesKeys[key]; }

 private:
  std::vector<T> values;
  std::priority_queue<T> valuesPriority;
  std::unordered_map<long long, T> valuesKeys;
};